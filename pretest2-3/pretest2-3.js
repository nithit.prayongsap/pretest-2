const request = require("request");
const cheerio = require("cheerio");
const argFundName = process.argv[2]

request({ url: 'https://codequiz.azurewebsites.net/', headers: { Cookie: "hasCookie=true;" } }, function (error, response, body) {
    if (error) {
        return
    }
    if (response.statusCode !== 200) {
        return
    }
    const $ = cheerio.load(body);
    let navIndex = 0
    $("body > table > tbody > tr > th").each((index, element) => {
        if ($(element).text().trim() === "Nav") {
            navIndex = index
        }
    });

    $("body > table > tbody > tr").each((index, element) => {
        const fundName = $(element).find("td").first().text()
        if (fundName.trim() === argFundName.trim()) {
            // console.log($(element).find("td").first().text());
            $(element).find("td").each((childIndex, childElement) => {
                if (childIndex === navIndex) {
                    console.log($(childElement).text());
                }
            })
        }

    });
});