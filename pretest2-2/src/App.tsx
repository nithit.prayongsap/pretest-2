import React, { useEffect, useState } from "react";
import "./App.css";

function App() {
  const [categories, setCategories] = useState<string[] | []>([]);
  const [textFilter, setTextFilter] = useState<string>("");
  useEffect(() => {
    fetch("https://api.publicapis.org/categories")
      .then((data) => data.json())
      .then((data) => setCategories(data));
  }, []);
  return (
    <div className="App">
      <div className="filter">
        <label>
          <b>Filter: </b>
        </label>
        <input
          type="text"
          value={textFilter}
          onChange={(e) => setTextFilter(e.target.value)}
        />
      </div>
      <table className="Table">
        <th>
          <td>Categories</td>
        </th>
        {categories
          .filter((data) => data.includes(textFilter))
          .map((data, i) => (
            <tr key={i}>
              <td>{data}</td>
            </tr>
          ))}
      </table>
    </div>
  );
}

export default App;
